use axum::{
    extract::{
        ws::{Message, WebSocket},
        State, WebSocketUpgrade,
    },
    http::Response,
    response::{Html, IntoResponse},
    routing::get,
    Router, Server,
};
use sysinfo::{CpuExt, System, SystemExt};
use tokio::sync::broadcast;

type Snapshot = Vec<f32>;

#[tokio::main]
async fn main() {
    let (tx, _) = broadcast::channel::<Snapshot>(1);

    let app_state = AppState { tx: tx.clone() };

    let router = Router::new()
        .route("/", get(index_html))
        .route("/realtime/cpus", get(realtime_cpus))
        .route("/index.js", get(index_js))
        .route("/index.css", get(index_css))
        .with_state(app_state.clone());

    tokio::task::spawn_blocking(move || {
        let mut sys = System::new();

        loop {
            sys.refresh_cpu();
            let _ = tx.send(sys.cpus().iter().map(CpuExt::cpu_usage).collect());

            std::thread::sleep(System::MINIMUM_CPU_UPDATE_INTERVAL);
        }
    });

    let server = Server::bind(&"0.0.0.0:1111".parse().unwrap()).serve(router.into_make_service());

    println!("Listening on: {}", server.local_addr());

    server.await.unwrap();
}

#[derive(Clone)]
struct AppState {
    tx: broadcast::Sender<Snapshot>,
}

async fn index_html() -> Html<String> {
    Html(tokio::fs::read_to_string("src/index.html").await.unwrap())
}

#[axum::debug_handler]
async fn realtime_cpus(
    State(app_state): State<AppState>,
    ws: WebSocketUpgrade,
) -> impl IntoResponse {
    ws.on_upgrade(|ws: WebSocket| async { realtime_cpus_stream(app_state, ws).await })
}

async fn realtime_cpus_stream(app_state: AppState, mut ws: WebSocket) {
    let mut rx = app_state.tx.subscribe();

    while let Ok(msg) = rx.recv().await {
        let payload = serde_json::to_string(&msg).unwrap();
        ws.send(Message::Text(payload)).await.unwrap();

        tokio::time::sleep(System::MINIMUM_CPU_UPDATE_INTERVAL).await;
    }
}

async fn index_js() -> Response<String> {
    Response::builder()
        .header("content-type", "application/javascript;charset=utf-8")
        .body(tokio::fs::read_to_string("src/index.js").await.unwrap())
        .unwrap()
}

async fn index_css() -> Response<String> {
    Response::builder()
        .header("content-type", "text/css;charset=utf-8")
        .body(tokio::fs::read_to_string("src/index.css").await.unwrap())
        .unwrap()
}
