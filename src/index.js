import { h, Component, render } from "https://unpkg.com/preact?module";
import htm from "https://unpkg.com/htm?module";

const html = htm.bind(h);

function App(props) {
    return html`
    <div>
    ${props.cpus.map((cpu) => {
        return html`
        <div class="bar">
            <div class="bar-inner" style="width: ${cpu}%;"></div>
            <label>${cpu.toFixed(2)}% usage</label>
        </div>
        `;
    })}
    </div>
    `;
}

// document.addEventListener("DOMContentLoaded", () => {
// 
//     setInterval(async () => {
//         const response = await fetch("/api/cpus");
//         if (response.status != 200)
//             throw new Error("HTTP error! status: ${response.status}");
//         const json = await response.json();
// 
//         render(html`<${App} cpus=${json}/>`, document.body);
//     }, 200);
// 
// })

let url = new URL("/realtime/cpus", window.location.href);
url.protocol = url.protocol.replace("http", "ws");
let ws = new WebSocket(url.href);
ws.onmessage = (ev) => {
    render(html`<${App} cpus=${JSON.parse(ev.data)}/>`, document.body);
}
