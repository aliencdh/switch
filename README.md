# switch

A web-based htop-like written as a practice project.

## What does it do?
It displays the usage of all of your CPU cores.

## Installation:

1. Install [Rust](https://www.rust-lang.org/tools/install)

2. Open a terminal and navigate to wherever you want this application to be downloaded to.

3. `git clone https://codeberg.org/aliencdh/switch`

4. `cd switch`

6. `cargo build --release`

## Usage:

From within the `switch` folder:

1. `cargo run --release`

2. Open a browser visit the following URL: `localhost:1111`
